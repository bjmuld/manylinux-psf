#! /usr/bin/env bash
set -exu

echo "compiling and installing libpsf"
cd /io/libpsf-core

./autogen.sh --enable-tests
make

cp src/.libs/*{.o,.a,.la,.so*} /usr/lib64/
cp include/*.h /usr/include/
