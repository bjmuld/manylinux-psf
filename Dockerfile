ARG BASE_IMAGE
FROM $BASE_IMAGE

# inject files
COPY . /io
RUN git clone https://gitlab.com/libpsf/libpsf-core.git /io/libpsf-core

## run script
RUN chmod +x /io/install_libpsf.sh
RUN /io/install_libpsf.sh

## cleanup
RUN rm -rf /io

